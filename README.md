## polybar-mullvad-wireguard
A small script to show whether you are connected to a Mullvad Wireguard server.
<img src="polybar-mullvad-wireguard.png" alt="polybar-mullvad-wireguard">

### Usage
1. Copy **polybar-mullvad-wireguard.sh** to **~/.config/polybar/scripts**.
2. Load the module from polybar.
```[module/polybar-mullvad-wireguard]
type = custom/script
interval = 2
format = <label>
exec = ~/.config/polybar/scripts/polybar-mullvad-wireguard.sh
```

### Notes
- I personally use the MaterialIcons font for my polybar icons, so I recommend that be installed, but any other font with icons should work.
- Feature requests are welcome.
